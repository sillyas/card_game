export const makeResponsiveCol = (breakpointsObj) => {
    let classString = 'col';

    Object.keys(breakpointsObj).forEach((key) => {
        let breakpoint = '-' + key;
        classString += ` col${breakpoint === '-all' ? '' : breakpoint}-${breakpointsObj[key]}`;
    });

    return classString;
};

export const arraysEqual = (array1,array2) => {
    return JSON.stringify(array1) === JSON.stringify(array2);
};

export const arrayShuffle = (array) => {
    let currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
};