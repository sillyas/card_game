import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import Notification, { NotificationContent, NotificationFooter } from 'cmps/Notification';
import { startGame } from 'actions';

class StartScreen extends Component {
  render() {
    return (
        <Notification>
            <NotificationContent>
                Hello, here you can start the game.<br/>
                Objective of the game is to match two cards<br/>
                with the same shirt. Now please get started.<br/>
                Just hit the "Start" button.
            </NotificationContent>
            <NotificationFooter>
                <Link to="/game" onClick={this.props.startGame} className="btn">Start game</Link>
            </NotificationFooter>
        </Notification>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    startGame
  }, dispatch)
};

export default connect(null, mapDispatchToProps)(StartScreen);
