import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Notification, { NotificationHeader, NotificationContent, NotificationFooter } from 'cmps/Notification';

class Error extends Component {
  render() {
    return (
      <Notification isError={true}>
          <NotificationHeader errorCode={this.props.errorCode}/>
          <NotificationContent>
              { this.props.errorText }
          </NotificationContent>
          <NotificationFooter>
              <Link to="/" className="btn">Go to Homepage</Link>
          </NotificationFooter>
      </Notification>
    );
  }
}

Error.propTypes = {
    errorCode: PropTypes.number.isRequired,
    errorText: PropTypes.string.isRequired
};

export default Error;
