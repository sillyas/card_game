import React, { Component } from 'react';

import Notification, { NotificationContent } from 'cmps/Notification';
import Cards from 'cmps/Cards';
import './GameField.scss';

class GameField extends Component {
    render() {
        return (
            <Notification className={"GameField-wrapper"}>
                <NotificationContent className={"row"}>
                    <Cards/>
                </NotificationContent>
            </Notification>
        );
    }
}

export default GameField;
