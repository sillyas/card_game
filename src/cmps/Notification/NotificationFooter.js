import React, { Component } from 'react';

import './Notification.scss';

class NotificationFooter extends Component {
    render() {
        return (
            <footer className="Notification-footer">
                { this.props.children }
            </footer>
        );
    }
}

export default NotificationFooter;
