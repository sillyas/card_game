import React, { Component } from 'react';
import classNames from 'classnames';

import './Notification.scss';

class NotificationContent extends Component {
    render() {
        let inheritClasses = typeof this.props.className === 'string' ? this.props.className.split(' ') : '',
            wrapperClass = classNames(
                'Notification-content',
                ...inheritClasses
            );

        return (
            <main className={wrapperClass}>
                { this.props.children }
            </main>
        );
    }
}

export default NotificationContent;
