import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Notification.scss';

class NotificationHeader extends Component {
    render() {
        return (
            <header className="Notification-header">
                { this.props.isError && 'Error' } { this.props.errorCode }
            </header>
        );
    }
}

NotificationHeader.propTypes = {
    isError: PropTypes.bool,
    errorCode: PropTypes.number
};

export default NotificationHeader;
