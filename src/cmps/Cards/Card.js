import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { flipCard, doPairMatched, doPairNotMatched } from 'actions';
import { CARD_FRONT_IMG } from 'common/const';
import { makeResponsiveCol } from 'common/func';

class Card extends Component {
    constructor() {
      super();

      this.onCardFlip = this.onCardFlip.bind(this);
      this.twoFlipped = function*() {
          let {
                  pairMatched,
                  ind1,
                  ind2,
                  doPairMatched,
                  doPairNotMatched
              } = yield,
              [ ...cardsArr ] = yield;

          if(pairMatched) {
              cardsArr[ind1].flipped = true;
              cardsArr[ind1].disabled = true;
              cardsArr[ind2].flipped = true;
              cardsArr[ind1].disabled = true;

              doPairMatched(cardsArr);
          } else {
              cardsArr[ind1].flipped = false;
              cardsArr[ind2].flipped = false;

              doPairNotMatched(cardsArr);
          }
      }
    }

    onCardFlip() {
      let {
          possiblePairInd,
          cardsArr,
          flipped,
          ind
        } = this.props,
        flipIndex = ind,
        firstFlipInPair = typeof possiblePairInd === 'undefined',
        pairMatched = !firstFlipInPair && cardsArr[flipIndex].src === cardsArr[possiblePairInd].src,
        afterFlipArr = this.props.cardsArr;

      afterFlipArr[flipIndex].flipped = !flipped;
      flipped = afterFlipArr[flipIndex].flipped;

      if(!firstFlipInPair){
            let twoFlipped = this.twoFlipped();
            twoFlipped.next();
            twoFlipped.next({
                ind1: possiblePairInd,
                ind2: flipIndex,
                pairMatched,
                doPairMatched,
                doPairNotMatched
            });

            setTimeout(
                (function() {
                    twoFlipped.next(this.props.cardsArr);
                }).bind(this),
                1500
            )
      }

      if(firstFlipInPair && flipped) {
        possiblePairInd = ind;
      } else {
        possiblePairInd = undefined;
      }

      this.props.flipCard(afterFlipArr, possiblePairInd);
    }

    render() {
        let { ind, src, flipped, disabled } = this.props;

        return (
            <article
              data-ind={ind}
              className={classNames(
                  'Card-wrapper',
                  { 'flipped': this.props.flipped },
                  makeResponsiveCol({ 'all': '12', 'sm': '6', 'lg': '4', 'xl': 3 })
              )}
              onClick={disabled ? () => ('') : this.onCardFlip}
            >
                <div className="Card-front">
                    <div
                      className="img-wrapper"
                      style={{ backgroundImage: `url(/assets/img/${CARD_FRONT_IMG})` }}
                    />
                </div>
                <div className="Card-back">
                    <div
                      className="img-wrapper"
                      style={{ backgroundImage: flipped && `url(/assets/img/${src})` }}
                    />
                </div>
            </article>
        );
    }
}

const mapStoreToProps = store => {
    let cardsArr = store.gameReducer.cardsArr && store.gameReducer.cardsArr.length ?
      [...store.gameReducer.cardsArr] : [],
      { possiblePairInd } = store.gameReducer;

    return {
      cardsArr,
      possiblePairInd
    };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    flipCard,
    doPairMatched,
    doPairNotMatched
  }, dispatch)
};

Card.propTypes = {
    ind: PropTypes.number.isRequired,
    src: PropTypes.string.isRequired,
    flipped: PropTypes.bool.isRequired,
    cardsArr: PropTypes.array,
    possiblePairInd: PropTypes.number
};

export default connect(mapStoreToProps, mapDispatchToProps)(Card);
