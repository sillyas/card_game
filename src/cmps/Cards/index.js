import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Card from './Card';
import './Cards.scss';
import { cardsMapped } from 'actions';
import { CARDS_IMG_ARR } from 'common/const';
import { arrayShuffle, arraysEqual } from 'common/func';

class Cards extends Component {
    constructor() {
        super();

        this.state = {cardsForRender:[]};
    }

    componentWillMount() {
        this.props.cardsMapped(this.mapCardsArray());
    }

    componentWillReceiveProps(nextProps) {
        if(!arraysEqual(this.props.cardsArr, nextProps.cardsArr)) {
          this.setState({cardsForRender: nextProps.cardsArr});
        }
    }

    mapCardsArray() {
        //We duplicating images from images array (for making pairs) and shuffling result
        let srcArray = [ ...CARDS_IMG_ARR, ...CARDS_IMG_ARR ].map( (src) => ({src}) ),
          shuffledArray = arrayShuffle(srcArray);

        return shuffledArray.map( (card, ind, arr) => ({
            src: card.src,
            flipped: false
        }));
    }

    render() {
        return (
            this.state.cardsForRender.map( (card, cardInd) => (
                <Card
                  key={cardInd}
                  ind={cardInd}
                  src={card.src}
                  flipped={card.flipped}
                  disabled={card.disabled}
                />
            )) || ''
        );
    }
}

const mapStoreToProps = store => {
    let cardsArr = store.gameReducer.cardsArr && store.gameReducer.cardsArr.length ?
      [...store.gameReducer.cardsArr] : [];

    return {
      cardsArr
    };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    cardsMapped
  }, dispatch)
};

export default connect(mapStoreToProps, mapDispatchToProps)(Cards);
