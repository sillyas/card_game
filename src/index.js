import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { AppContainer } from 'react-hot-loader';

// redux deps
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import reducer from './reducers';
import Routes from './routes';
import 'common/scss/index.scss';

const middleware = [thunk];

const store = createStore(
  reducer,
  applyMiddleware(...middleware),
);

const root = document.getElementById('root');

const render = () => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <Routes/>
      </Provider>
    </AppContainer>,
    root,
  );
};

render();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
