import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import ProtectedRoute from './ProtectedRoute';
import StartScreen from 'cmps/StartScreen';
import GameField from 'cmps/GameField';
import App from 'cmps/App';

class Routes extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <ProtectedRoute
            path="/game"
            component={GameField}
            condition={this.props.inGame}
          />
          <ProtectedRoute
            path="/win"
            component={App}
            condition={true}
          />
          <ProtectedRoute
            path="/lose"
            component={App}
            condition={true}
          />
          <Route path="/" component={StartScreen}/>
        </Switch>
      </Router>
    )
  }
}

const mapStoreToProps = store => {
  return {
    inGame: store.gameReducer.inGame,
    win: store.gameReducer.win,
    lose: store.gameReducer.lose
  };
};

export default connect(mapStoreToProps)(Routes);