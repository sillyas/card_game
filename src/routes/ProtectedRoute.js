import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Error from 'cmps/Error';

class ProtectedRoute extends Component {
  render() {
    const {
      component: Component,
      // eslint-disable-next-line
      condition: condition,
      ...props
    } = this.props;

    return (
      <Route
        {...props}
        render={props => (
          condition ?
            <Component {...props} /> :
            <Error
              errorCode={403}
              errorText={"You're not allowed to be here!"}
            />
        )}
      />
    )
  }
}

export default ProtectedRoute;