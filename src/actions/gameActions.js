import {
  GO_TO_GAME,
  CARDS_MAPPED,
  CARD_FLIPPED,
  PAIR_MATCHED,
  PAIR_NOT_MATCHED,
  WON,
  LOST
} from './types';

export const startGame = () => ({
  type: GO_TO_GAME,
  inGame: true,
  won: true,
  lost: true
});

export const cardsMapped = (cardsArr) => ({
  type: CARDS_MAPPED,
  cardsArr
});

export const flipCard = (cardsArr, possiblePairInd) => ({
  type: CARD_FLIPPED,
  cardsArr,
  possiblePairInd
});

export const doPairMatched = (cardsArr) => ({
    type: PAIR_MATCHED,
    cardsArr
});

export const doPairNotMatched = (cardsArr) => ({
    type: PAIR_NOT_MATCHED,
    cardsArr
});

export const wonGame = () => ({
  type: WON,
  inGame: false,
  won: true
});

export const lostGame = () => ({
  type: LOST,
  inGame: false,
  lost: true
});