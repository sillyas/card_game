import * as actions from 'actions/types';

const defaultGameState = {
  cardsArr: [],
  possiblePairInd: undefined,
  inGame: false,
  won: false,
  lose: false
};

export default function gameReducer(state = defaultGameState, action) {
  switch (action.type) {
    case actions.GO_TO_GAME:
      return {...state,inGame:action.inGame};
    case actions.CARDS_MAPPED:
      return {...state,cardsArr:action.cardsArr};
    case actions.CARD_FLIPPED:
      return {...state,cardsArr:action.cardsArr,possiblePairInd:action.possiblePairInd};
    case action.PAIR_MATCHED:
      return {...state,cardsArr:action.cardsArr};
    case action.PAIR_NOT_MATCHED:
      return {...state,cardsArr:action.cardsArr};
    case actions.WON:
      return {...state,inGame:action.inGame,won:action.won};
    case actions.LOST:
      return {...state,inGame:action.inGame,lost:action.lost};
    default:
      return state;
  }
}